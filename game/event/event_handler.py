from sys import exit
from typing import Callable, Tuple

import pygame

from .actions import Actions
from ..helpers import ExitEvent, Score


class EventHandler:

    actions = Actions()
    clock = pygame.time.Clock()
    exit_event = ExitEvent()
    score = Score()


    def __init__(self) -> None:
        self._target_events = [pygame.KEYUP, pygame.KEYDOWN]

    def run_action(self, *actions:Tuple[Callable[..., None]]):
        self.actions.run_custom(*actions)

    def register(self, type:int, key:int, action:Callable[..., None]) -> None:
        self.actions.add(type, key, action)

    def unregister(self, action:Callable[..., None]) -> None:
        self.actions.remove(action)

    def _stop(self) -> None:
        self.exit_event.set()
        exit(0)

    def handle(self) -> None:
        try:
            for event in pygame.event.get(eventtype=self._target_events):
                self.actions.run(event.type, event.key)
            for event in pygame.event.get([pygame.QUIT]):
                self._stop()
        except: self._stop()
