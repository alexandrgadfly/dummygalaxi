from threading import Thread
from typing import Callable, Tuple


class Actions:

    def _attr_name(self, type:int, key:int) -> str:
        return f'_{type}_{key}'

    def add(self, type:int, key:int, action:Callable[..., None]) -> None:
        k = self._attr_name(type, key)
        if not hasattr(self, k):
            setattr(self, k, [])
        attr:list = getattr(self, k)
        if action not in attr:
            attr.append(action)

    def remove(self, action:Callable[..., None]):
        actions = list(self.__dict__.values())
        try:
            if action in actions:
                index = actions.index(action)
                key = list(self.__dict__.keys())[index]
                delattr(self, key)
        except:...

    def run(self, type:int, key:int) -> None:
        k = self._attr_name(type, key)
        actions = getattr(self, k, [])
        for action in actions:
            try: Thread(target=action).start()
            except:...

    def run_custom(self, *actions:Tuple[Callable[..., None]]) -> None:
        for action in actions:
            try: Thread(target=action).start()
            except: ...
