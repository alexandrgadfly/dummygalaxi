import pygame

from ..config import Config
from ..elements import Elements
from ..event import EventHandler


class AlienInvasion:
    event_handler = EventHandler()

    def __init__(self) -> None:
        pygame.init()
        pygame.display.set_caption(Config.main_window_title)

        self.screen = pygame.display.set_mode(Config.main_window_size.as_tuple)
        self.elements = Elements(self.screen, self.event_handler)

    def run(self):
        while True:
            self.event_handler.handle()
            self.screen.fill(color=Config.bg_color.as_tuple)
            self.elements.update()
            pygame.display.update()
            self.event_handler.clock.tick(Config.FPS)
