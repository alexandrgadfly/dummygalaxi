import pygame
from pathlib import Path


class ImageStorage:

    files = Path(__file__).resolve().parent.joinpath('files')
    # ship = pygame.image.load(files.joinpath('ship.bmp'))
    # alien = pygame.image.load(files.joinpath('alien.bmp'))
    ship = pygame.image.load(files.joinpath('ship.png'))
    alien = pygame.image.load(files.joinpath('alien.png'))
    background = pygame.image.load(files.joinpath('bg.png'))
