from pygame.transform import scale, smoothscale
from pygame.surface import Surface
from ..config import Position, Config
from ..event import EventHandler
from ..images import ImageStorage
from .base import IElement, IElements
from ..logger import Log


class Background(IElement):

    def __init__(self, screen: Surface, event_handler: EventHandler, parent: IElements, position: Position = Position()) -> None:
        super().__init__(screen, event_handler, parent, position)
        self._layer = -1

    def setup(self) -> None:
        self.image = smoothscale(ImageStorage.background.convert_alpha(), self.screen.get_size())
        self.rect = self.image.get_rect()

    def blitme(self) -> None:
        self.screen.blit(self.image, self.screen_rect)