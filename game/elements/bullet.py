import pygame
from pygame.surface import Surface
from pygame.sprite import Sprite
from pygame.rect import Rect
from ..event import EventHandler
from .base import IElement, IElements
from ..config import Config, Position
from ..logger import Log


class Bullet(IElement, Sprite):

    def __init__(self, screen: Surface, event_handler: EventHandler, parent: IElements, position: Position = Position()) -> None:
        super().__init__(screen, event_handler, parent, position)

    def setup(self) -> None:
        self.rect = Rect(
            self.position.x,
            self.position.y,
            Config.bullet.width,
            Config.bullet.width
        )
        # self.parent.add(self)
        self.eh.run_action(self.a_move)

    def a_move(self) -> None:
        while self.rect.bottom > 0:
            self.rect.y -= Config.bullet.speed
            self.eh.clock.tick(Config.FPS)
        self.destroy()

    def blitme(self) -> None:
        pygame.draw.rect(self.screen, Config.bullet.color.as_tuple, self.rect)