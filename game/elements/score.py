from pygame.surface import Surface
from pygame.font import Font, SysFont

from ..config import Position, Config
from ..event import EventHandler
from .base import IElement, IElements


class ScoreBoard(IElement):

    def __init__(self, screen: Surface, event_handler: EventHandler, parent: IElements, position: Position = Position()) -> None:
        super().__init__(screen, event_handler, parent, position)

    def setup(self) -> None:
        self.rect = self.image.get_rect()
        self.rect.top = 20
        self.rect.right = self.screen_rect.right - 50

    @property
    def _font(self) -> Font:
        return SysFont(None, Config.score.text_font_size)

    @property
    def image(self) -> Surface:
        current = self.eh.score.current
        return self._font.render(
            str(current),
            True,
            Config.score.text_color.as_tuple,
            Config.score.bg_color.as_tuple
        )

    def blitme(self) -> None:
        element = self.image
        self.screen.blit(element, self.rect)
