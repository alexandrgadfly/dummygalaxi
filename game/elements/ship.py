from pygame import K_LEFT, K_RIGHT, K_SPACE, KEYDOWN, KEYUP
from pygame.surface import Surface

from ..config import Config, Position
from ..event import EventHandler
from ..helpers import Action, synchronize
from ..images import ImageStorage
from ..logger import Log
from .base import IElement, IElements
from .bullet import Bullet


class Ship(IElement):

    def __init__(self, screen:Surface, event_handler:EventHandler, parent:IElements, position:Position = Position()) -> None:
        super().__init__(screen, event_handler, parent, position)
        self._key_pressed = False

    def _setup_actions(self) -> None:
        actions = [
            Action(KEYUP, K_RIGHT, self.a_key_release),
            Action(KEYDOWN, K_RIGHT, self.a_move_right),

            Action(KEYUP, K_LEFT, self.a_key_release),
            Action(KEYDOWN, K_LEFT, self.a_move_left),

            Action(KEYDOWN, K_SPACE, self.a_shoot),
        ]
        self.actions.extend(actions)

    def setup(self) -> None:
        self._setup_actions()
        self.image = ImageStorage.ship.convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.midbottom = self.screen_rect.midbottom

    @synchronize
    def a_key_press(self) -> None:
        self._key_pressed = True

    @synchronize
    def a_key_release(self) -> None:
        self._key_pressed = False

    def a_move_right(self) -> None:
        self.a_key_press()
        while self._key_pressed and self.rect.right < self.screen_rect.right:
            self.rect.x += Config.ship.speed
            self.eh.clock.tick(Config.FPS)

    def a_move_left(self) -> None:
        self.a_key_press()
        while self._key_pressed and self.rect.left > self.screen_rect.left:
            self.rect.x -= Config.ship.speed
            self.eh.clock.tick(Config.FPS)

    def a_shoot(self) -> None:
        if self.parent.elements_count(Bullet) < Config.ship.max_bullets:
            bullet = Bullet(self.screen, self.eh, self.parent, Position(*self.rect.midtop))
            self.parent.add(bullet)
