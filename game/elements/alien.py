from random import randint
import signal
import pygame
from pygame.surface import Surface

from ..config import Position, Config
from ..event import EventHandler
from .base import IElement, IElements
from ..images import ImageStorage
from .bullet import Bullet
from ..logger import Log


class Alien(IElement):

    def __init__(self, screen: Surface, event_handler: EventHandler, parent: IElements, position: Position = Position()) -> None:
        super().__init__(screen, event_handler, parent, position)

    def setup(self) -> None:
        self.image = ImageStorage.alien.convert_alpha()
        self.rect = self.image.get_rect()
        self.rect.x = 10
        self.rect.y = randint(20, int(self.screen_rect.bottom/2))
        self.eh.run_action(self.a_move)

    def _create_new(self) -> None:
        alien_n = Alien(self.screen, self.eh, self.parent, Position())
        self.parent.add(alien_n)

    def _process_hit(self, element:IElement) -> None:
        self.eh.score.hit
        self.destroy()
        element.destroy()
        self._create_new()

    @property
    def bullet_hit(self) -> bool:
        c_list = self.rect.collideobjectsall(self.parent.get_elements(self))
        for element in c_list:
            if isinstance(element, Bullet):
                self._process_hit(element)
                return True
        return False


    def a_move(self) -> None:
        direction = 1
        while True:
            self.rect.x += Config.alient.speed*direction
            border = (
                (self.screen_rect.right - self.rect.right) < 10,
                (self.rect.left - self.screen_rect.left) < 10
            )
            if any(border): direction *= -1
            if self.eh.exit_event.is_set(): break
            if self.bullet_hit: break
            self.eh.clock.tick(Config.FPS)