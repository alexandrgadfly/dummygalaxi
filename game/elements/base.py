from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List

from pygame.rect import Rect
from pygame.surface import Surface

from ..event import EventHandler
from ..helpers import Action
from ..config import Position
from ..logger import Log


class IElement(ABC):
    image: Surface
    rect: Rect
    actions:List[Action]

    @abstractmethod
    def setup(self) -> None:...

    def __init__(self, screen:Surface, event_handler:EventHandler, parent:IElements, position:Position = Position()) -> None:
        self._layer = 100
        self.actions = []
        self.screen = screen
        self.eh = event_handler
        self.parent = parent
        self.position = position
        self.screen_rect = self.screen.get_rect()
        self.setup()
        self.register_actions()

    def __repr__(self) -> str:
        return f'<{self.__class__.__name__} object at {self.__class__.__hash__(self)}>'

    def register_actions(self) -> None:
        for action in self.actions:
            self.eh.register(*action)

    def destroy(self) -> None:
        for action in self.actions:
            self.eh.unregister(action.callback)
        self.parent.remove(self)

    def blitme(self) -> None:
        self.screen.blit(self.image, self.rect)

class IElements(ABC):
    @abstractmethod
    def update(self) -> None:...
    @abstractmethod
    def add(self, element:IElement) -> None:...
    @abstractmethod
    def remove(self, element:IElement) -> None:...
    @abstractmethod
    def get_elements(self, element:IElement) -> List[IElement]:...
    @abstractmethod
    def elements_count(self, T:IElement) -> int:...