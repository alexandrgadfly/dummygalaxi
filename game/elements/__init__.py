from typing import List

from pygame import Surface

from game.elements.base import IElement

from ..config import Position
from ..event import EventHandler
from .alien import Alien
from .background import Background
from .base import IElement, IElements
from .score import ScoreBoard
from .ship import Ship
from ..logger import Log

USED_ELEMENTS = {
    Background, Ship, Alien, ScoreBoard
}


class Elements(IElements):

    def __init__(self, screen: Surface, event_handler:EventHandler) -> None:
        self.container:List[IElement] = []
        for element in USED_ELEMENTS:
            self.container.append(element(screen, event_handler, self))
        self._sort_container()

    def _sort_container(self) -> None:
        self.container.sort(key=lambda i: i._layer)

    def update(self):
        for element in self.container:
            element.blitme()

    def add(self, element: IElement) -> None:
        if element not in self.container:
            self.container.append(element)
        self._sort_container()

    def remove(self, element:IElement) -> None:
        if element in self.container:
            self.container.remove(element)

    def get_elements(self, element: IElement) -> List[IElement]:
        container_c = self.container.copy()
        if element in container_c: container_c.remove(element)
        return container_c

    def elements_count(self, T:IElement) -> int:
        elements = tuple(filter(lambda e: isinstance(e, T), self.container))
        return len(elements)
