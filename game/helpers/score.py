from .locker import synchronize
from ..config import Config


class Score:
    def __init__(self) -> None:
        self.__score = 0

    @property
    @synchronize
    def hit(self) -> None:
        self.__score += Config.alient.cost

    @property
    @synchronize
    def cleanup(self) -> None:
        self.__score = 0

    @property
    @synchronize
    def current(self) -> int:
        return self.__score


