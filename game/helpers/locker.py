from threading import Lock
from typing import Callable, ParamSpec, TypeVar
from ..logger import Log

T = TypeVar('T')
P = ParamSpec('P')
LOCK = Lock()


def synchronize(func:Callable[P, T]) -> Callable[P, T]:
    def wrapper(*args, **kwargs) -> T:
        LOCK.acquire()
        r = func(*args, **kwargs)
        LOCK.release()
        return r
    return wrapper
