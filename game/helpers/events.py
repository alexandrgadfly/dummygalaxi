from __future__ import annotations
from threading import Event


class Singleton(object):
    _instance = None
    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

class ExitEvent(Singleton, Event):
    def __init__(self) -> None:
        super().__init__()