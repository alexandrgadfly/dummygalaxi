from dataclasses import dataclass
from typing import Callable, Iterator


@dataclass
class Action:
    type:int
    key:int
    callback:Callable[..., None]

    def __iter__(self) -> Iterator:
        yield self.type
        yield self.key
        yield self.callback