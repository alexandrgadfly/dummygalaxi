from logging import DEBUG
from pathlib import Path

from .objects import Color, Resolution


class ScoreBoardConfig:
    text_color = Color(0,191,255)
    bg_color = Color(0,4,37)
    text_font_size = 48

class LogConfig:
    name = 'game.log'
    file = Path(__file__).resolve().parent.parent.joinpath(name)
    level = DEBUG

class AlienConfig:
    speed = 5
    cost = 10

class BulletConfig:
    speed = 5
    width = 7
    height = 30
    color = Color(0,191,255)

class ShipConfig:
    speed = 5
    max_bullets = 3

class Config:
    log = LogConfig
    main_window_size = Resolution(1200, 800)
    main_window_title = 'Dummy Galaxy'
    bg_color = Color(255, 255, 255)
    FPS = 60
    ship = ShipConfig
    bullet = BulletConfig
    alient = AlienConfig
    score = ScoreBoardConfig