from dataclasses import dataclass
from .base import Base


@dataclass
class Color(Base):
    red:int
    green:int
    blue:int
    opacity:int = 1