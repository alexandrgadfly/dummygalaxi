from typing import Union


class Base:

    def __getitem__(self, key:Union[str, int, slice]):
        if isinstance(key, (int, slice)):
            return tuple(self.__dict__.values())[key]
        raise TypeError(f'{self.__class__.__name__} object is not subscriptable')

    @property
    def as_tuple(self) -> tuple:
        return tuple(self.__dict__.values())