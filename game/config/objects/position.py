from dataclasses import dataclass
from .base import Base


@dataclass
class Position(Base):
    x:int = 0
    y:int = 0