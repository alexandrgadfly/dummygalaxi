from __future__ import annotations
from dataclasses import dataclass
from typing import Union
from .base import Base


@dataclass
class Resolution(Base):
    width:int
    height:int

    def get_enlarged(self, scale:Union[int, float]) -> Resolution:
        return Resolution(
            width=int(self.width * scale),
            height=int(self.height * scale)
        )