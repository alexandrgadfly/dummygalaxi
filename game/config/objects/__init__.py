from .color import Color
from .resolution import Resolution
from .position import Position