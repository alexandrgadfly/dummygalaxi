from logging import Logger, Formatter, StreamHandler, FileHandler
from ..config import Config



class LogBase(Logger):

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(Config.log.name, Config.log.level)
        self.set_stream_handler()

    @property
    def formatter(self) -> Formatter:
        return Formatter(
            fmt="%(asctime)s - [%(levelname)s] - %(message)s"
        )

    def set_stream_handler(self) -> None:
        handler = StreamHandler()
        handler.setFormatter(self.formatter)
        handler.setLevel(Config.log.level)
        self.addHandler(handler)


class Log(metaclass=LogBase):...