# DummyGalaxi

## Launching

``` bash
python3 -m venv ./env

source ./env/bin/activate

pip install -r ./requirements.txt

python3 ./main.py
```

## Actions

| **Action** |      **Key**      |
|-----------:| :-----------------|
| movement   | left-right arrows |
| shoot      | Spacebar          |
